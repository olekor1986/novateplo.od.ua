<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    protected $users = [
        [
            'name' => 'o.korovenko',
            'role' => 'admin',
            'email' => 'olekor1986@gmail.com',
            'password' => '123456789Qw'
        ],
        [
            'name' => 'y.shalar',
            'role' => 'admin',
            'email' => 'y.shalar@gmail.com',
            'password' => '123456789Qw'
        ],
        [
            'name' => 'user',
            'role' => 'user',
            'email' => 'user@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'guest',
            'role' => 'guest',
            'email' => 'guest@mail.com',
            'password' => '123456Qw'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [];
        foreach ($this->users as $key => $value){
            $array[$key]['name'] = $value['name'];
            $array[$key]['role'] = $value['role'];
            $array[$key]['email'] = $value['email'];
            $array[$key]['password'] = Hash::make($value['password']);
        }
        DB::table('users')->insert($array);
    }
}
