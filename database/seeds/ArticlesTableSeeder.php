<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticlesTableSeeder extends Seeder
{
    protected $array  = [
        [
            'title' => 'Ваши квадрокруги — неправильные',
            'slug' => 'vashi-kvadrokrugi-nepravilnye',
            'body' => 'На днях вышла статья, посвящённая разнице между квадратом со скруглёнными краями и «квадрокругом» — промежуточной фигурой между окружностью и квадратом, полученной из формулы cуперэллипса. Мнения читателей разделились — не все увидели разницу, а кто увидели — не все отдали предпочтение «правильному» варианту. И я подозреваю, почему: эти ваши квадрокруги — ненастоящие!',
            'videos' => '["https:\\/\\/youtube.com\\/embed\\/zejUIUrAcIw","https:\\/\\/youtube.com\\/embed\\/zejUIUrAcIw"]',
            'user_id' => 1,
            'created_at' => '2020-09-04 03:05:24',
            'updated_at' => '2020-09-04 03:05:24'
        ],
        [
            'title' => 'Who is mr. Marvin?',
            'slug' => 'who-is-mr-marvin',
            'body' => 'В июне этого года мы выпустили на рынок умную колонку с голосовым помощником по имени Марвин. Она может работать до 2 часов без подзарядки. У колонки шесть микрофонов для обработки голосовых команд. ПО полностью разработано внутри МТС без использования сторонних коммерческих решений. В этом посте разберем основные функции и технические характеристики новинки.',
            'videos' => '["https:\\/\\/youtube.com\\/embed\\/mQe94eoEmbY","https:\\/\\/youtube.com\\/embed\\/mQe94eoEmbY"]',
            'user_id' => 1,
            'created_at' => '2020-09-04 03:07:47',
            'updated_at' => '2020-09-04 03:07:47'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert($this->array);
    }
}
