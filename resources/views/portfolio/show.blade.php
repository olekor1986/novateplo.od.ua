@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li><a href="/portfolio">Примеры работ</a></li>
        <li class="active">{{ $portfolio->title }}</li>
    </ol>
    <div class="row">
        <!-- Article main content -->
        <article class="col-sm-9 maincontent">
            <header class="page-header">
                <h1 class="page-title">{{ $portfolio->title }}</h1>
                <h5 class="h5">{{ $portfolio->real_date->diffForHumans() }}</h5>
            </header>
            <p class="text-muted">{{ $portfolio->body }}</p>
        </article>
    </div>
    @if(count($portfolio->portfolio_image))
    <div class="slides">
        <ul class="pgwSlideshow">
            @foreach($portfolio->portfolio_image as $image)
                <li><img src="{{ asset('uploads/portfolio/' . $image->image) }}" alt=""></li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
    @if(count($portfolio->videos))
    <div id="video" class="container text-center">
        @foreach($portfolio->videos as $video)
            <iframe width="560" height="315" src="{{ $video }}" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
        @endforeach
    </div>
    @endif
<script type="text/javascript">
        $(document).ready(function() {
            $('.pgwSlideshow').pgwSlideshow({
                autoSlide: true,
                maxHeight: 450
            });
        });
</script>
@endsection
