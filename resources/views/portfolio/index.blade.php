@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
    <h3 class="h3">Примеры работ</h3>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @if(count($portfolios))
                <ul class="list-group">
                    @foreach($portfolios as $portfolio)
                        <li class="list-group-item">
                            <a href="/portfolio/{{$portfolio->slug}}">{{$portfolio->title}}</a>
                        </li>
                    @endforeach
                </ul>
                @else
                    <p>Примеров работ пока нет</p>
                @endif
            </div>
        </div>
    </div>
@endsection
