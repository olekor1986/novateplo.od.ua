<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="author"      content="Олег Коровенко">
    <meta name="description" content="Новая теплосеть. Теплоснабжение. Автоматизация. Диспетчеризация. Насосные станции. Договор. Безналичный расчет. Наличный расчет. Жмите!">
    <meta name="title" content="Новая теплосеть. Подготовка проекта. Монтаж. Наладка. Теплоснабжение. Водоснабжение. Электроснабжение.">
    <meta name="email" content="novateplo@i.ua">
    <meta name="phone" content="+380989920182">

    <title>Новая теплосеть - делаем теплоснабжение лучше</title>

    <link rel="canonical" href="http://www.novateplo.od.ua">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Custom styles for our template -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.css') }}" media="screen" >
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <style>
        .card {
            margin: 50px;

        }
    </style>

</head>

<body class="">
@yield('header')

@yield('content')


<div class="container">
    <div class="text-center">
        <a href="/" class="btn btn-lg btn-dark">Вернуться на главную</a>
    </div>
</div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    </body>
</html>
