<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="author"      content="Олег Коровенко">
    <meta name="description" content="Новая теплосеть. Теплоснабжение. Автоматизация. Диспетчеризация. Насосные станции. Договор. Безналичный расчет. Наличный расчет. Жмите!">
    <meta name="title" content="Новая теплосеть. Подготовка проекта. Монтаж. Наладка. Теплоснабжение. Водоснабжение. Электроснабжение.">
    <meta name="email" content="novateplo@i.ua">
    <meta name="phone" content="+380989920182">


    <title>Новая теплосеть - делаем теплоснабжение лучше</title>

    <link rel="canonical" href="http://www.novateplo.od.ua">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <!-- Custom styles for our template -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.css') }}" media="screen" >
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <!-- 2Gis Map -->
    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>

    <!-- PgwSlideshow -->
    <link rel='stylesheet' href='https://cdn.rawgit.com/Pagawa/PgwSlideshow/master/pgwslideshow.min.css'>
    <script src="https://code.jquery.com/jquery-1.11.0.min.js" integrity="sha256-spTpc4lvj4dOkKjrGokIrHkJgNA0xMS98Pw9N7ir9oI=" crossorigin="anonymous"></script>
    <script src='https://cdn.rawgit.com/Pagawa/PgwSlideshow/master/pgwslideshow.min.js'></script>

    <!-- Custom styles -->
    <style>
        .row {
            margin: 15px;
        }
        .list-group-item {
            font-size: large;
        }
        #last_jobs{
            background-color: #eff1bb;
        }

    </style>
    <!-- Jivo Site -->
    <script src="//code.jivosite.com/widget/R5darFvgbk" async></script>
    <!-- /Jivo Site -->
</head>

<body class="home">
<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle"
                    data-toggle="collapse"
                    data-target=".navbar-collapse"
                    aria-controls="navbarResponsive"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ asset('assets/images/logo.png') }}" alt="Novateplo"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li><a href="/">Главная</a></li>
                <li><a href="/portfolio">Примеры работ</a></li>
                <li><a href="/articles">Статьи</a></li>
                <!--
                    <li><a href="/contacts">Контакты</a></li>
                    <li><a href="/monitoring">Мониторинг</a></li>
                -->
                @if(Auth::check())
                    @if(Auth::user()->role == 'admin')
                        <li><a class="text-success" href="/admin">Администратор</a></li>
                    @endif
                @endif
                @if(Auth::check())
                    <li><a class="text-primary" href="/users/{{Auth::user()->id}}/edit">{{Auth::user()->name}}</a></li>
                    <li>
                        <form action="/logout" method="post">
                            @csrf
                            <button class="btn" role="button">Выход</button>
                        </form>

                    </li>
                @else
                    <!--
                        <li><a class="btn" href="/login">Войти</a></li>
                    -->
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<!-- /.navbar -->

<!-- Header -->
@yield('header')
<!-- /Header -->
@yield('content')


<!-- Social links. @TODO: replace by link/instructions in template -->
<section id="social">
    <div class="container">
        <div class="wrapper clearfix">
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style">
                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                <a class="addthis_button_tweet"></a>
                <a class="addthis_button_linkedin_counter"></a>
                <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
            </div>
            <!-- AddThis Button END -->
        </div>
    </div>
</section>
<!-- /social links -->


<footer id="footer" class="top-space">

    <div class="footer1">
        <div class="container">
            <div class="row">

                <div class="col-md-3 widget">
                    <h3 class="widget-title">Контактная информация</h3>
                    <div class="widget-body">
                        <p>+38 098 992 01 82<br>
                            <a href="mailto:#">novateplo@i.ua</a><br>
                            <br>
                        </p>
                    </div>
                </div>

                <div class="col-md-3 widget">
                    <h3 class="widget-title">Мы в соцсетях</h3>
                    <div class="widget-body">
                        <p class="follow-me-icons">
                            <a href=""><i class="fa fa-telegram fa-2"></i></a>
                            <a href=""><i class="fa fa-instagram fa-2"></i></a>
                            <a href=""><i class="fa fa-facebook fa-2"></i></a>
                        </p>
                    </div>
                </div>

                <div class="col-md-6 widget">
                    <h3 class="widget-title">Заявка на консультацию</h3>
                    <div class="widget-body">
                        <p>Для заказа выездной консультации оставьте заявку в всплывающем окне</p>
                        <p>Консультации по телефону - бесплатно</p>
                    </div>
                </div>

            </div> <!-- /row of widgets -->
        </div>
    </div>

    <div class="footer2">
        <div class="container">
            <div class="row">

                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="simplenav">
                            <a href="/">Главная</a> |
                            <a href="/portfolio">Примеры работ</a> |
                            <a href="/articles">Статьи</a> |
                            {{--
                            <a href="/contacts">Контакты</a> |
                            <a href="/monitoring">Мониторинг</a> |
                            --}}
                            @if(!Auth::check())
                                <b><a href="/login">ВХОД</a></b>
                            @endif
                        </p>
                    </div>
                </div>

                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="text-right">
                            Copyright &copy; 2020, NOVATEPLO. Designed by Oleg Korovenko
                        </p>
                    </div>
                </div>

            </div> <!-- /row of widgets -->
        </div>
    </div>

</footer>

<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
-->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/headroom.min.js') }}"></script>
<script src="{{ asset('assets/js/jQuery.headroom.min.js') }}"></script>
<script src="{{ asset('assets/js/template.js') }}"></script>
</body>
</html>
