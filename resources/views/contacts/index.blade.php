@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
    <!-- container -->
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="active">Контакты</li>
        </ol>

        <div class="row">

            <!-- Article main content -->
            <article class="col-sm-9 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Напишите нам</h1>
                </header>

                <p>
                    Мы хотели бы услышать Вас. Заинтересованы в совместной работе?
                    Заполните форму ниже, указав некоторую информацию о Вашей проблеме, и мы свяжемся с Вами.
                </p>
                <br>
                <form>
                    <div class="row">
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Имя">
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Email">
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Телефон">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea placeholder="Напишите сообщение здесь..." class="form-control" rows="9"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox"><input type="checkbox"> Подписаться на рассылку</label>
                        </div>
                        <div class="col-sm-6 text-right">
                            <input class="btn btn-action" type="submit" value="Send message">
                        </div>
                    </div>
                </form>

            </article>
            <!-- /Article -->

            <!-- Sidebar -->
            <aside class="col-sm-3 sidebar sidebar-right">

                <div class="widget">
                    <h4>Адрес</h4>
                    <address>

                    </address>
                    <h4>Телефон:</h4>
                    <address>
                        +38 098 992 01 82
                    </address>
                </div>

            </aside>
            <!-- /Sidebar -->

        </div>
    </div>	<!-- /container -->

    <section class="container-full top-space">
        <div id="map"></div>
    </section>

    <script type="text/javascript">
        var map;

        DG.then(function () {
            map = DG.map('map', {
                center: [46.490046, 30.749258],
                zoom: 13
            });

            DG.marker([46.490046, 30.749258]).addTo(map);
        });
    </script>
@endsection
