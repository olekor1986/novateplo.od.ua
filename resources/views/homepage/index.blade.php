@extends('templates.progressus')

@section('header')
<header id="head">
    <div class="container">
        <div class="row">
            <h1 class="lead">НАДЕЖНОСТЬ, ПРОФЕССИОНАЛИЗМ, ЭФФЕКТИВНОСТЬ</h1>
            <p class="tagline">НОВАЯ ТЕПЛОСЕТЬ: делаем теплоснабжение лучше</p>
        </div>
    </div>
</header>
@endsection

@section('content')
    <!-- Intro -->
    <div class="container text-center">
        <br> <br>
        <h2 class="thin">НОВАЯ ТЕПЛОСЕТЬ — это группа инженеров и мастеров, работающих по направлениям</h2>
        <p class="text-muted">
            энергосбережение, теплоснабжение, электроснабжение, автоматизация и диспетчеризация, водоснабжение и насосные станции
        </p>
    </div>
    <!-- /Intro-->

    <!-- Highlights - jumbotron -->
    <div class="jumbotron top-space">
        <div class="container">

            <h3 class="text-center thin">Кое-что из нашей деятельности, но далеко не все)</h3>

            <div class="row">
                <div class="col-md-3 col-sm-6 highlight">
                    <div class="h-caption"><h4><i class="fa fa-thermometer-full fa-5" aria-hidden="true"></i>Теплосчетчики</h4></div>
                    <div class="h-body text-center">
                        <p>
                            Для Вас установим приборы учета тепла только проверенных производителей,
                            внесенные в госреестр средств измерения, таких как Landis&Gyr, Kamstrup, Sempal. Также мы
                            предлагаем услуги по проведению госповерки прибора учета.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 highlight">
                    <div class="h-caption"><h4><i class="fa fa-flash fa-5"></i>Электромонтаж</h4></div>
                    <div class="h-body text-center">
                        <p>
                            Произведем все виды электромонтажных работ, а именно: ремонт и замена электропроводки, монтаж светильников, ремонт оборудования электрощитовых, монтаж и ремонт пускорегулирующей аппаратуры, подключение элеткрооборудования.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 highlight">
                    <div class="h-caption"><h4><i class="fa fa-bath fa-5"></i>Водоснабжение</h4></div>
                    <div class="h-body text-center">
                        <p>
                            Установка подкачки холодной воды необходима для повышения давления в системе водоразбора
                            высотных зданий. Установка оснащается баками-гидроаккумуляторами и автоматикой, благодаря чему повышается ресурс насосов и снижается расход электроэнергии.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 highlight">
                    <div class="h-caption"><h4><i class="fa fa-smile-o fa-5"></i>Техподдержка клиентов</h4></div>
                    <div class="h-body text-center">
                        <p>
                            Осуществляем техническую поддержку наших клиентов и консультирование по
                            вопросам взаимодействия с теплоснабжающей организацией
                        </p>
                    </div>
                </div>
            </div> <!-- /row  -->

        </div>
    </div>
    <!-- /Highlights -->

    <!-- Last Jobs -->
    @if(count($lastPortfolios))
        <div id="last_jobs" class="album py-5">
            <div class="container">
                <h3 class="text-center thin">Наши недавние работы</h3>
                <div class="row">
                    @foreach($lastPortfolios as $portfolio)
                    <div class="col-md-4">
                        <div class="card mb-4 text-center">
                             @if(count($portfolio->portfolio_image))
                                <img src="{{ asset('uploads/portfolio/' . $portfolio->portfolio_image[0]->image) }}"
                                     class="card-img-top" alt="" height="200px">
                            @endif
                            <div class="card-body">
                                <h5 class="card-title">{{ $portfolio->title }}</h5>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="{{ '/portfolio/' . $portfolio->slug }}" role="button"
                                           class="btn btn-sm btn-default">Просмотр</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <!-- /Last Jobs -->

    <!-- container -->
    <div class="container">
        <h3 class="text-center thin">МОНИТОРИНГ И ДИСПЕТЧЕРИЗАЦИЯ</h3>
        <div class="h-body text-center">
            <p>
                Мы предоставляем возможность получения параметров работы объекта теплоснабжения,
                работающего без постоянного персонала. Результаты можно вывести на схему или построить график.
            </p>
            <img src="/assets/images/chart_preview_1.jpg" style="width: 600px">
        </div>

    </div>	<!-- /container -->
@endsection
