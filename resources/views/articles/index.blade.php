@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
    <h3 class="h3">Статьи</h3>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @if(count($articles))
                <ul class="list-group">
                    @foreach($articles as $article)
                        <li class="list-group-item">
                            <a href="/articles/{{$article->slug}}">{{$article->title}}</a>
                        </li>
                    @endforeach
                </ul>
                @else
                    <p>Статей пока нет</p>
                @endif
            </div>
        </div>
    </div>
@endsection
