@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary">
    </header>
@endsection

@section('content')
    <div class="container">
        <h3 class="h3">Панель администратора</h3>
        <div class="row">
            <div class="col-md-3">
                <div class="btn-group-vertical">
                    <a href="/admin/articles" role="button" class="btn btn-sm btn-default">Статьи</a>
                    <a href="/admin/portfolio" role="button" class="btn btn-sm btn-default">Примеры работ</a>
                    <a href="/admin/users" role="button" class="btn btn-sm btn-default">Пользователи</a>
                    <a href="#" role="button" class="btn btn-sm btn-default">Link4</a>
                </div>
            </div>
        </div>
    </div>
@endsection
