@extends('templates.empty')

@section('content')
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li><a href="/admin/users">Пользователи</a></li>
            <li class="active">Создать нового пользователя</li>
        </ol>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Создать нового пользователя') }}</div>
                        <div class="card-body">
                            <form enctype="multipart/form-data" method="POST" action="/admin/users">
                                @csrf
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Логин') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="name" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('E-mail') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="email" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Временный пароль') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="password" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Роль') }}</label>
                                    <div class="col-md-6">
                                        <select name="role" id="" required>
                                            <option selected disabled hidden>Выбрать роль...</option>
                                            <option value="admin">admin</option>
                                            <option value="user">user</option>
                                            <option value="guest">guest</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="first_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Фамилия') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="last_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Название компании') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="company">
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Создать') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
