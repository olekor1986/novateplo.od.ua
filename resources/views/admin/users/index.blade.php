@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
    <h3 class="h3">Пользователи</h3>
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li class="active">Пользователи</li>
        </ol>
        <div class="row">
            <div class="col-md-4">
                <a role="button" class="btn btn-sm btn-primary" href="/admin/users/create">Добавить пользователя</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                @if(count($users))
                    <table class="table table-responsive table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Логин</th>
                                <th>E-mail</th>
                                <th>Роль</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Компания</th>
                                <th>Создан</th>
                                <th>Изменен</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->last_name }}</td>
                                <td>{{ $user->company }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->updated_at }}</td>
                                <td>
                                    <a href="/admin/users/{{$user->id}}/edit" role="button"
                                       class="btn btn-sm btn-warning">Edit</a>
                                </td>
                                <td>
                                    <form action="/admin/users/{{$user->id}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">Del</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
