@extends('templates.empty')

@section('content')
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li><a href="/admin/portfolio">Примеры работ</a></li>
            <li class="active">Добавить новую работу в портфолио</li>
        </ol>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Добавить новую работу в портфолио') }}</div>
                        <div class="card-body">
                            <form enctype="multipart/form-data" method="POST" action="/admin/portfolio">
                                @csrf
                                <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}" >
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Заголовок') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="body" class="col-md-4 col-form-label text-md-right">{{ __('Текст') }}</label>
                                    <div class="col-md-6">
                                        <textarea id="body" type="text" class="form-control" name="body"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="real_date" class="col-md-4 col-form-label text-md-right">{{ __('Действительная дата') }}</label>
                                    <div class="col-md-6">
                                        <input id="real_date" type="date" class="form-control" name="real_date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="images" class="col-md-4 col-form-label text-md-right">{{ __('Изображения') }}</label>
                                    <div id="imagesDynamicInput"  class="col-md-6">
                                        <input id="images" type="file" name="images[]">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <input type="button" value="Добавить поле" onClick="addImageInput('imagesDynamicInput');">
                                    </div>
                                </div>
                                <script>
                                    var imageCounter = 1;
                                    var imageLimit = 7;
                                    function addImageInput(divName){
                                        if (imageCounter == imageLimit)  {
                                            alert("Можно загрузить не более " + imageCounter + " изображений");
                                        }
                                        else {
                                            var newdiv = document.createElement('div');
                                            newdiv.innerHTML = "Image " + (imageCounter + 1) + " <br><input id='images' type='file' name='images[]'>";
                                            document.getElementById(divName).appendChild(newdiv);
                                            imageCounter++;
                                        }
                                    }
                                </script>
                                <div class="form-group row">
                                    <label for="videos" class="col-md-4 col-form-label text-md-right">{{ __('Видео') }}</label>
                                    <div id="videoDynamicInput"  class="col-md-6">
                                        <input id="videos" type="text" name="videos[]" placeholder="Youtube ссылка">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <input type="button" value="Добавить поле" onClick="addVideoInput('videoDynamicInput');">
                                    </div>
                                </div>
                                <script>
                                    var videoCounter = 1;
                                    var videoLimit = 2;
                                    function addVideoInput(divName){
                                        if (videoCounter == videoLimit)  {
                                            alert("Можно загрузить не более  " + videoCounter + " видео");
                                        }
                                        else {
                                            var newdiv = document.createElement('div');
                                            newdiv.innerHTML = "Video " + (videoCounter + 1) + " <br><input id='videos' type='text' name='videos[]' placeholder='Youtube ссылка'>";
                                            document.getElementById(divName).appendChild(newdiv);
                                            videoCounter++;
                                        }
                                    }
                                </script>
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Создать') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
