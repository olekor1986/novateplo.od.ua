@extends('templates.empty')

@section('content')
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li><a href="/admin/portfolio">Примеры работ</a></li>
            <li><a href="{{ '/admin/portfolio/' . $portfolioImage->portfolio->slug . '/edit' }}">{{'Edit ' . $portfolioImage->portfolio->title}}</a></li>
            <li class="active">{{ $portfolioImage->image }}</li>
        </ol>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Replace image') }}</div>
                        <div class="card-body">
                            <form enctype="multipart/form-data" method="POST" action="/admin/portfolio_image/{{ $portfolioImage->id }}">
                                @csrf
                                @method('put')
                                <input type="hidden" name="old_image_id" value="{{ $portfolioImage->id }}">
                                <input type="hidden" name="old_image_name" value="{{ $portfolioImage->image }}">
                                <input type="hidden" name="portfolio_id" value="{{ $portfolioImage->portfolio_id }}">
                                <input type="hidden" name="slug" value="{{ $portfolioImage->portfolio->slug }}">
                                <div class="form-group row">
                                    <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>
                                    <div id="imagesDynamicInput"  class="col-md-6">
                                        <input id="image" type="file" name="image">
                                    </div>
                                </div>
                                <img src="{{ asset('uploads/portfolio/' . $portfolioImage->image) }}" alt="">
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Replace') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
