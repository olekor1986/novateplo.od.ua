@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
    <h3 class="h3">Примеры работ</h3>
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li class="active">Примеры работ</li>
        </ol>
        <div class="row">
            <div class="col-md-4">
                @if(Auth::user()->role == 'admin')
                    <a role="button" class="btn btn-sm btn-primary" href="/admin/portfolio/create">Создать портфолио</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                @if(count($portfolios))
                    <table class="table table-responsive table-bordered">
                    <tbody>
                        @foreach($portfolios as $portfolio)
                            <tr>
                                <td>{{ $portfolio->id }}</td>
                                <td>{{ $portfolio->real_date }}</td>
                                <td><a href="/admin/portfolio/{{$portfolio->slug}}">{{$portfolio->title}}</a></td>
                                <td><a href="/admin/portfolio/{{$portfolio->slug}}/edit" role="button"
                                       class="btn btn-sm btn-warning">Edit</a></td>
                                <td>
                                    <form action="/admin/portfolio/{{$portfolio->slug}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">Del</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <p>Примеров работ пока нет</p>
                @endif
            </div>
        </div>
    </div>
@endsection
