@extends('templates.empty')

@section('content')
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li><a href="/admin/portfolio">Примеры работ</a></li>
            <li class="active">{{ 'Edit ' . $portfolio->title }}</li>
        </ol>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Edit portfolio' . ' ' . $portfolio->title) }}</div>
                        <div class="card-body">
                            <form method="post" action="/admin/portfolio/{{ $portfolio->slug }}">
                                @csrf
                                @method('put')
                                <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}" >
                                <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>
                                    <div class="col-md-6">
                                        <input id="title" type="text" class="form-control" name="title" value="{{ $portfolio->title }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="body" class="col-md-4 col-form-label text-md-right">{{ __('Body') }}</label>
                                    <div class="col-md-6">
                                        <textarea id="body" type="text" class="form-control" name="body">
                                            {{ $portfolio->body }}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="real_date" class="col-md-4 col-form-label text-md-right">{{ __('Действительная дата') }}</label>
                                    <div class="col-md-6">
                                        <input id="real_date" type="date" class="form-control" name="real_date" value="{{ $portfolio->real_date }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="videos" class="col-md-4 col-form-label text-md-right">{{ __('Videos') }}</label>
                                    <div class="col-md-6">
                                        @foreach($portfolio->videos as $video)
                                            <input id="videos" type="text" name="videos[]" placeholder="Youtube video link" value="{{ $video }}">
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Update') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <div class="card">
                    <div class="card-header">{{ __('Edit images') }}</div>
                        <div class="card-body">
                            @foreach($portfolio->portfolio_image as $image)
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{ __('Image' . ' ' . $image->image) }}</label>
                                    <div class="col-md-8">
                                        <img src="{{ asset('uploads/portfolio/' . $image->image) }}" alt="" height="100px">
                                        <form action="/admin/portfolio_image/{{$image->id}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-sm btn-danger">Del</button>
                                        </form>
                                        <a href="/admin/portfolio_image/{{ $image->id }}/edit" role="button" class="btn btn-sm btn-warning">Edit</a>
                                    </div>
                                </div>
                            @endforeach
                            <form enctype="multipart/form-data" method="POST" action="/admin/portfolio_image">
                                @csrf
                                <input type="hidden" name="portfolio_id" value="{{ $portfolio->id }}">
                                <input type="hidden" name="slug" value="{{ $portfolio->slug }}">
                                <div class="form-group row">
                                    <label for="images" class="col-md-4 col-form-label text-md-right">{{ __('Изображения') }}</label>
                                    <div id="imagesDynamicInput"  class="col-md-6">
                                        <input id="images" type="file" name="images[]">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <input type="button" value="Добавить поле" onClick="addImageInput('imagesDynamicInput');">
                                    </div>
                                </div>
                                <script>
                                    var imageCounter = 1;
                                    var imageLimit = 7;
                                    function addImageInput(divName){
                                        if (imageCounter == imageLimit)  {
                                            alert("Можно загрузить не более " + imageCounter + " изображений");
                                        }
                                        else {
                                            var newdiv = document.createElement('div');
                                            newdiv.innerHTML = "Image " + (imageCounter + 1) + " <br><input id='images' type='file' name='images[]'>";
                                            document.getElementById(divName).appendChild(newdiv);
                                            imageCounter++;
                                        }
                                    }
                                </script>
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Add') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
                </div>
            </div>
        </div>
@endsection
