@extends('templates.empty')

@section('content')
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li><a href="/admin/articles">Статьи</a></li>
            <li><a href="{{ '/admin/articles/' . $articleImage->article->slug . '/edit' }}">{{'Edit ' . $articleImage->article->title}}</a></li>
            <li class="active">{{ $articleImage->image }}</li>
        </ol>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Replace image') }}</div>
                        <div class="card-body">
                            <form enctype="multipart/form-data" method="POST" action="/admin/article_image/{{ $articleImage->id }}">
                                @csrf
                                @method('put')
                                <input type="hidden" name="old_image_id" value="{{ $articleImage->id }}">
                                <input type="hidden" name="old_image_name" value="{{ $articleImage->image }}">
                                <input type="hidden" name="article_id" value="{{ $articleImage->article_id }}">
                                <input type="hidden" name="slug" value="{{ $articleImage->article->slug }}">
                                <div class="form-group row">
                                    <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>
                                    <div id="imagesDynamicInput"  class="col-md-6">
                                        <input id="image" type="file" name="image">
                                    </div>
                                </div>
                                <img src="{{ asset('uploads/articles/' . $articleImage->image) }}" alt="">
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Replace') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
