@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li><a href="/admin">Администратор</a></li>
        <li><a href="/admin/articles">Статьи</a></li>
        <li class="active">{{ $article->title }}</li>
    </ol>
    <div class="row">
        <!-- Article main content -->
        <article class="col-sm-9 maincontent">
            <header class="page-header">
                <h1 class="page-title">{{ $article->title }}</h1>
            </header>
            <p class="text-muted">{{ $article->body }}</p>
        </article>
    </div>
    @if(count($article->article_image))
    <div class="slides">
        <ul class="pgwSlideshow">
            @foreach($article->article_image as $image)
                <li><img src="{{ asset('uploads/articles/' . $image->image) }}" alt=""></li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
    @if(count($article->videos))
    <div id="video" class="container text-center">
        @foreach($article->videos as $video)
            <iframe width="560" height="315" src="{{ $video }}" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
        @endforeach
    </div>
    @endif
<script type="text/javascript">
        $(document).ready(function() {
            $('.pgwSlideshow').pgwSlideshow({
                autoSlide: true,
                maxHeight: 450
            });
        });
</script>
@endsection
