@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
    <h3 class="h3">Статьи</h3>
    <!-- container -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/admin">Администратор</a></li>
            <li class="active">Статьи</li>
        </ol>
        <div class="row">
            <div class="col-md-4">
                @if(Auth::user()->role == 'admin')
                    <a role="button" class="btn btn-sm btn-primary" href="/admin/articles/create">Создать статью</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                @if(count($articles))
                    <table class="table table-responsive table-bordered">
                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td>{{ $article->id }}</td>
                                <td>{{ $article->created_at }}</td>
                                <td><a href="/admin/articles/{{$article->slug}}">{{$article->title}}</a></td>
                                <td><a href="/admin/articles/{{$article->slug}}/edit" role="button"
                                       class="btn btn-sm btn-warning">Edit</a>
                                </td>
                                <td>
                                    <form action="/admin/articles/{{$article->slug}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">Del</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p>Статей пока нет</p>
                @endif
            </div>
        </div>
    </div>
@endsection
