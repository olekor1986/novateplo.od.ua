@extends('templates.progressus')

@section('header')
    <header id="head" class="secondary"></header>
@endsection
@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active">{{ 'Данные пользователя ' .  $user->first_name . ' ' . $user->last_name }}</li>
    </ol>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="/users/{{ $user->id }}">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Фамилия') }}</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Название компании') }}</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="company" value="{{ $user->company }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('E-mail') }}</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Изменить данные') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <a role="button" class="btn btn-success" href="/password/reset">Сбросить пароль</a>
</div>
@endsection
