<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'slug', 'body', 'images', 'videos', 'user_id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function article_image()
    {
        return $this->hasMany(ArticleImage::class);
    }

}
