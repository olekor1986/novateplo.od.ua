<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = ['title', 'slug', 'body', 'images', 'videos', 'user_id', 'real_date'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function portfolio_image()
    {
        return $this->hasMany(PortfolioImage::class);
    }
}
