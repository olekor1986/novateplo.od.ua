<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioImage extends Model
{
    protected $fillable = ['portfolio_id', 'image'];

    public function portfolio()
    {
        return $this->belongsTo(Portfolio::class);
    }

    static public function saveImages($imageFiles, $portfolioId)
    {
        $imgArray = [];
        foreach($imageFiles as $image){
            $uploadedFile = $image->move(public_path() . '/uploads/portfolio/',
                'work_' . date('Y-m-d_H-i-s') . crc32($image->getClientOriginalName()) .
                '.' . $image->getClientOriginalExtension());
            $imgArray['portfolio_id'] = $portfolioId;
            $imgArray['image'] = $uploadedFile->getBasename();
            PortfolioImage::create($imgArray);
        }
    }
    static public function deleteImageFile($image)
    {
        $imagePath = public_path() . '/uploads/portfolio/' . $image;
        (new \Illuminate\Filesystem\Filesystem)->delete($imagePath);
    }
}
