<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleImage extends Model
{
    protected $fillable = ['article_id', 'image'];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    static public function saveImages($imageFiles, $articleId)
    {
        $imgArray = [];
        foreach($imageFiles as $image){
            $uploadedFile = $image->move(public_path() . '/uploads/articles/',
                date('Y-m-d_H-i-s') . crc32($image->getClientOriginalName()) .
                '.' . $image->getClientOriginalExtension());
            $imgArray['article_id'] = $articleId;
            $imgArray['image'] = $uploadedFile->getBasename();
            ArticleImage::create($imgArray);
        }
    }
    static public function deleteImageFile($image)
    {
        $imagePath = public_path() . '/uploads/articles/' . $image;
        (new \Illuminate\Filesystem\Filesystem)->delete($imagePath);
    }
}
