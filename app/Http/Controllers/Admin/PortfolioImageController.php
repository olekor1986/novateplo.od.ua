<?php

namespace App\Http\Controllers\Admin;

use App\ArticleImage;
use App\Http\Controllers\Controller;
use App\Portfolio;
use App\PortfolioImage;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PortfolioImageController extends Controller
{
    public function  __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $imgFiles = $request->file('images');
        $portfolioData = $request->only('portfolio_id', 'slug');
        $portfolio_id = $portfolioData['portfolio_id'];
        if ($request->hasFile('images')){
            PortfolioImage::saveImages($imgFiles, $portfolio_id);
        }
        return back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param PortfolioImage $portfolioImage
     * @return Response
     */
    public function edit(PortfolioImage $portfolioImage)
    {
        return view('admin.portfolio.images.edit', compact('portfolioImage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param PortfolioImage $portfolioImage
     * @return Response
     */
    public function update(Request $request, PortfolioImage $portfolioImage)
    {
        $imgData = $request->only('old_image_id', 'old_image_name', 'portfolio_id', 'slug');
        $imgFiles[] = $request->file('image');
        PortfolioImage::saveImages($imgFiles, $imgData['portfolio_id']);
        ArticleImage::deleteImageFile($imgData['old_image_name']);
        $portfolioImage->find($imgData['old_image_id'])->delete();
        return redirect('/admin/portfolio/' . $imgData['slug'] . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PortfolioImage $portfolioImage
     * @return Response
     * @throws \Exception
     */
    public function destroy(PortfolioImage $portfolioImage)
    {
        ArticleImage::deleteImageFile($portfolioImage->image);
        $portfolioImage->delete();
        return back();
    }
}
