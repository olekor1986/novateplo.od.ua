<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ArticleImage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ArticleImageController extends Controller
{
    public function  __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param ArticleImage $articleImage
     * @return Response
     */
    public function edit(ArticleImage $articleImage)
    {
        return view('admin.articles.images.edit', compact('articleImage'));
    }


    public function store(Request $request)
    {
        $imgFiles = $request->file('images');
        $articleData = $request->only('article_id', 'slug');
        $article_id = $articleData['article_id'];
        if ($request->hasFile('images')){
            ArticleImage::saveImages($imgFiles, $article_id);
        }
        return back();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param ArticleImage $articleImage
     * @return void
     */
    public function update(Request $request, ArticleImage $articleImage)
    {
        $imgData = $request->only('old_image_id', 'old_image_name', 'article_id', 'slug');
        $imgFiles[] = $request->file('image');
        ArticleImage::saveImages($imgFiles, $imgData['article_id']);
        ArticleImage::deleteImageFile($imgData['old_image_name']);
        $articleImage->find($imgData['old_image_id'])->delete();
        return redirect('/admin/articles/' . $imgData['slug'] . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ArticleImage $articleImage
     * @return Response
     * @throws Exception
     */
    public function destroy(ArticleImage $articleImage)
    {
        ArticleImage::deleteImageFile($articleImage->image);
        $articleImage->delete();
        return back();
    }
}
