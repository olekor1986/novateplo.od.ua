<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Portfolio;
use App\PortfolioImage;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminPortfolioController extends Controller
{
    public function  __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $portfolios = Portfolio::all()
            ->sortByDesc('real_date');
        return view('admin.portfolio.index', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.portfolio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $portfolioCreateData = $request->except('images');
        $portfolioCreateData['slug'] = Str::slug($portfolioCreateData['title']);
        $portfolioCreateData['videos'] = json_encode($portfolioCreateData['videos']);
        Portfolio::create($portfolioCreateData);
        $portfolio_id = DB::table('portfolios')
            ->orderByDesc('created_at')
            ->select('id')
            ->first()->id;
        $imgFiles = $request->file('images');
        if ($request->hasFile('images')){
            PortfolioImage::saveImages($imgFiles, $portfolio_id);
        }
        return redirect('/admin/portfolio');
    }

    /**
     * Display the specified resource.
     *
     * @param Portfolio $portfolio
     * @return Response
     */
    public function show(Portfolio $portfolio)
    {
        $portfolio['videos'] = json_decode($portfolio['videos'], 'true');
        return view('admin.portfolio.show', compact('portfolio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Portfolio $portfolio
     * @return Response
     */
    public function edit(Portfolio $portfolio)
    {
        $portfolio['videos'] = json_decode($portfolio['videos'], true);
        $realDate = Carbon::parse($portfolio['real_date']);
        $portfolio['real_date'] = $realDate->isoFormat('YYYY-MM-DD');
        return view('admin.portfolio.edit', compact('portfolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Portfolio $portfolio
     * @return Response
     */
    public function update(Request $request, Portfolio $portfolio)
    {
        $portfolioUpdateData = $request->all();
        $portfolioUpdateData['slug'] = Str::slug($portfolioUpdateData['title']);
        $portfolioUpdateData['videos'] = json_encode($portfolioUpdateData['videos']);
        $portfolio->update($portfolioUpdateData);
        return redirect('/admin/portfolio/' . $portfolioUpdateData['slug'] . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Portfolio $portfolio
     * @return Response
     * @throws Exception
     */
    public function destroy(Portfolio $portfolio)
    {
        $images = PortfolioImage::all()
            ->where('portfolio_id', 'like', $portfolio->id);
        foreach($images as $image){
            PortfolioImage::deleteImageFile($image->image);
            $image->delete();
        }
        $portfolio->delete();
        return redirect('/admin/portfolio');
    }
}
