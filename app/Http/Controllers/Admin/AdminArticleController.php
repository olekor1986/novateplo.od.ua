<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Article;
use App\ArticleImage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminArticleController extends Controller
{
    public function  __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $articles = Article::all()
            ->sortByDesc('created_at');
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $articleCreateData = $request->except('images');
        $articleCreateData['slug'] = Str::slug($articleCreateData['title']);
        $articleCreateData['videos'] = json_encode($articleCreateData['videos']);
        Article::create($articleCreateData);
        $article_id = DB::table('articles')
            ->orderByDesc('created_at')
            ->select('id')
            ->first()->id;

        $imgFiles = $request->file('images');
        if ($request->hasFile('images')){
            ArticleImage::saveImages($imgFiles, $article_id);
        }
        return redirect('/admin/articles');
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return Response
     */
    public function show(Article $article)
    {
        $article['videos'] = json_decode($article['videos'], 'true');
        return view('admin.articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Article $article
     * @return Response
     */
    public function edit(Article $article)
    {
        $article['images'] = json_decode($article['images'], true);
        $article['videos'] = json_decode($article['videos'], true);
        return view('admin.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Article $article
     * @return void
     */
    public function update(Request $request, Article $article)
    {
        $articleUpdateData = $request->all();
        $articleUpdateData['slug'] = Str::slug($articleUpdateData['title']);
        $articleUpdateData['videos'] = json_encode($articleUpdateData['videos']);
        $article->update($articleUpdateData);
        return redirect('/admin/articles/' . $articleUpdateData['slug'] . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return void
     * @throws \Exception
     */
    public function destroy(Article $article)
    {
        $images = ArticleImage::all()
            ->where('article_id', 'like', $article->id);
        foreach($images as $image){
            ArticleImage::deleteImageFile($image->image);
            $image->delete();
        }
        $article->delete();
        return redirect('/admin/articles');
    }
}
