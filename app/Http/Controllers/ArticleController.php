<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Response;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $articles = Article::all()
            ->sortByDesc('created_at');
        return view('articles.index', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return Response
     */
    public function show(Article $article)
    {
        $article['videos'] = json_decode($article['videos'], 'true');
        return view('articles.show', compact('article'));
    }


}
