<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all()
            ->sortByDesc('real_date');
        return view('portfolio.index', compact('portfolios'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        $portfolio['videos'] = json_decode($portfolio['videos'], 'true');
        $realDate = Carbon::parse($portfolio['real_date']);
        $portfolio['real_date'] = $realDate->locale('ru');
        return view('portfolio.show', compact('portfolio'));
    }

}
