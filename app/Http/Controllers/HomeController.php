<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lastPortfolios = Portfolio::all()
            ->sortByDesc('real_date')
            ->take(3)
            ->load('portfolio_image');
        return view('homepage.index', compact('lastPortfolios'));
    }
}
