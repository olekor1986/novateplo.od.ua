<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index');

/** Contacts page temporary disabled **/
/*
    Route::get('/contacts', function(){
        return view('contacts.index');
    });
*/
Auth::routes(['register' => false]);

Route::resource('articles', 'ArticleController', [ 'only' => [
    'index', 'show']]);
Route::resource('portfolio', 'PortfolioController', [ 'only' => [
    'index', 'show']]);
Route::resource('users', 'UserController', [ 'only' => [
    'edit', 'update']]);

/** Admin Routes **/
Route::get('/admin', 'Admin\AdminHomeController@index');
Route::resource('admin/articles', 'Admin\AdminArticleController');
Route::resource('admin/article_image', 'Admin\ArticleImageController', ['only' => ['edit',
    'create', 'store', 'update', 'destroy']]);
Route::resource('admin/portfolio', 'Admin\AdminPortfolioController');
Route::resource('admin/portfolio_image', 'Admin\PortfolioImageController', ['only' => ['edit',
    'create', 'store', 'update', 'destroy']]);
Route::resource('admin/users', 'Admin\AdminUserController', ['only' => ['index', 'edit',
    'create', 'store', 'update', 'destroy']]);
